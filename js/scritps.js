// cars for sale

const cars = [
  {
    carId: 1,
    imageSrc: "images/polo.jpeg",
    carName: "Volkswagen Polo",
    price: "12000",
    modelYear: "2018 Model Year",
    mileage: "22,000 miles",
    drive: "Manual",
    emission: "Petrol",
    doors: "5 doors",
    previousOwners: "2 previous owners",
  },
  {
    carId: 2,
    imageSrc: "images/corsa.jpg",
    carName: "Vauxhall Corsa",
    price: "7500",
    modelYear: "2019 Model Year",
    mileage: "33,000 miles",
    drive: "Manual",
    emission: "Petrol",
    doors: "5 doors",
    previousOwners: "1 previous owner",
  },
  {
    carId: 3,
    imageSrc: "images/fiesta.jpg",
    carName: "Ford Fiesta",
    price: "9000",
    modelYear: "2019 Model Year",
    mileage: "18,000 miles",
    drive: "Manual",
    emission: "Petrol",
    doors: "5 doors",
    previousOwners: "1 previous owner",
  },
  {
    carId: 4,
    imageSrc: "images/range.jpg",
    carName: "Range Rover",
    price: "14000",
    modelYear: "2016 Model Year",
    mileage: "37,000 miles",
    drive: "Manual",
    emission: "Petrol",
    doors: "5 doors",
    previousOwners: "3 previous owners",
  },
  {
    carId: 5,
    imageSrc: "images/mercedes.jpg",
    carName: "Mercedes Benz",
    price: "7000",
    modelYear: "2014 Model Year",
    mileage: "51,000 miles",
    drive: "Automatic",
    emission: "Diseal",
    doors: "5 doors",
    previousOwners: "4 previous owners",
  },
  {
    carId: 6,
    imageSrc: "images/focus.jpg",
    carName: "Ford Focus",
    price: "16000",
    modelYear: "2020 Model Year",
    mileage: "8,000 miles",
    drive: "Manual",
    emission: "Petrol",
    doors: "5 doors",
    previousOwners: "1 previous owners",
  },
];

// to set the hamburger when on an iphone - used for all html pages

if (document.querySelector(".hamburger")) {
  document.querySelector(".hamburger").addEventListener("click", function () {
    document.querySelector(".main-nav").classList.toggle("open");
  });
}

// JS for index html

if ($(".carousel").length) {
  $(".carousel").slick({
    autoplay: true,
    autoplaySpeed: 3000,
    dots: true,
    prevArrow:
      '<div class="slick-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></div>',
    nextArrow:
      '<div class="slick-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></div>',
  });
}

// JS for buy used html

for (var i = 0; i < cars.length; i++) {
  var car = `  <div class="car-card">
  <div class="card-image" style="background: url('${cars[i].imageSrc}') no-repeat center center; background-size: cover;">
  </div>
  <ul>
    <li style="font-weight: bold;"> ${cars[i].carName}</li>
    <li>£ ${cars[i].price}</li>
    <li>${cars[i].modelYear}</li>
    <li>${cars[i].mileage}</li>
    <li>${cars[i].previousOwners}</li>
  </ul>
  <button class="buy-button" type="button" car-id=${cars[i].carId}>Buy</button>
</div>`;
  $(".flex-container").append(car);
}

if ($(".buy-button").length) {
  $(".buy-button").click(function () {
    let carId = $(this).attr("car-id");
    location.href = "buy-now.html?car=" + carId;
  });
}

// JS for buy now html

let params = new URL(document.location).searchParams;
let selectedCarId = params.get("car");
cars.forEach((e) => {
  if (e.carId == selectedCarId) {
    var car = `<div class="car-image" style="background: url('${e.imageSrc}') no-repeat center center; background-size: cover;">
    </div>
    <div>
    <ul>
      <li style="font-weight: bold;"> ${e.carName}</li>
      <li>${e.modelYear}</li>
      <li>${e.mileage}</li>
      <li>${e.drive}</li>
      <li>${e.emission}</li>
      <li>${e.doors}</li>
      <li>${e.previousOwners}</li>
    </ul>
  </div>`;
    $(".car").append(car);
  }
});

if ($(".car-price").length) {
  cars.forEach((e) => {
    if (e.carId == selectedCarId) {
      $(".car-price").html(`£${e.price}`);
    }
  });
}

// calculation to work out monthly payments

if ($(".calculate-button").length) {
  $(".calculate-button").click(function () {
    let carPrice;
    let carDeposit;
    let paymentMonths;
    let priceMinusDeposit;
    let monthlyPrice;
    cars.forEach((e) => {
      if (e.carId == selectedCarId) {
        carPrice = e.price;
      }
    });
    carDeposit = $("input[type=number][name=deposit]").val();
    paymentMonths = $("#months").find(":selected").text();
    if (parseInt(carPrice) > parseInt(carDeposit)) {
      priceMinusDeposit = carPrice - carDeposit;
      monthlyPrice = priceMinusDeposit / paymentMonths;
      monthlyPrice = (Math.round(monthlyPrice * 100) / 100).toFixed(2);
      let finalPrice = `<h4> Monthly Payments of £ ${monthlyPrice} </h4>`;
      $(".final-price").html(finalPrice);
      $(".buy-now").css("display", "block");
    } else {
      let error = `<h4 style="color:Red;"> Please enter a deposit amount less than the value of the car! </h4>`;
      $(".final-price").html(error);
      if (jQuery(".buy-now").css("display") == "block") {
        $(".buy-now").css("display", "none");
      }
    }
  });
}
